package Home_Work_Figure_Exceptions;

public class Triangle extends Figure {
    private int a;
    private int b;
    private int c;
    private int h;

    public void setA(int a) {
        if(a>0) {
            this.a = a;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }

    public void setB(int b) {
        if(b>0) {
            this.b = b;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }

    public void setC(int c) {
        if(c>0) {
            this.c = c;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }
    public void setH(int h) {
        if (h > 0) {
            this.h = h;
        } else {
            System.out.println("negative value is not valid\nvalue =0");
        }

    }

    @Override
    protected void perimetr() {
        System.out.println("Triangle perimetr = "+(a+b+c));
    }

    @Override
    protected void area() {
        System.out.println("Triangle area = "+(a*h)/2);
    }
}
