package Home_Work_Figure_Exceptions;

public class Rectangle extends Square {
    private int a;
    private  int b;

    public void setA(int a) {
        if(a>0) {
            this.a = a;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }

    public void setB(int b) {
        if(b>0) {
            this.b = b;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }

    @Override
    protected void perimetr() {
        System.out.println("Rectangle perimetr = "+(a+b)*2);
    }

    @Override
    protected void area() {
        System.out.println("Rectangle area = "+a*b);
    }
}
