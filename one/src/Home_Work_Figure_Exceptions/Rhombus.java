package Home_Work_Figure_Exceptions;

public class Rhombus extends Square {
    private int h;
    public void setH(int h) {
        if(h>0) {
            this.h = h;
        }else {
            System.out.println("negative value is not valid\nvalue =0");
        }
    }

    @Override
    protected void area() {
        System.out.println("Rhombus area = "+getOneLine()*h);
    }
}
