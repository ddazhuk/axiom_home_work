package Home_Work_Figure_Exceptions;

public class Square extends Figure{
    private int oneLine;
    Square(){};
    Square(int a){
        this.oneLine=a;
    }
    public void setOneSquareLine(int oneSquareLine) {
        if(oneSquareLine>0) {
            this.oneLine = oneSquareLine;
        }else {
            System.out.println("negative value is not valid\nLine=0");
        }
    }

    public int getOneLine() {
        return oneLine;
    }

    @Override
    protected void perimetr() {
        System.out.println("Perimetr = "+(float)4*this.oneLine);
    }
    public void perimetr( int a){
        System.out.println("Perimetr = "+(float)4*a);
    }

    @Override
    protected void area() {
        System.out.println("Square area = "+oneLine*oneLine);
    }
}
