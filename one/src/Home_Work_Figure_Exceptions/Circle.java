package Home_Work_Figure_Exceptions;

public class Circle extends Figure {
    private int radius;

    Circle(){};
    Circle(int r) throws IllegalAccessException {
        this.radius=r;
        if(r<0){
            throw new IllegalAccessException();
        }
    }

    @Override
    public void area() {
        System.out.println("Circle are = "+(float)3.1415*(radius*radius));
    }

    @Override
    public void perimetr() {
        System.out.println("Circle perimetr = "+(float)2*3.1415*radius);
    }

    public void setRadius(int radius) {
        if(radius>0) {
            this.radius = radius;
        }else {
            System.out.println("negative value is not valid\nRadius=0");
        }

    }
}
