package Home_Work_Annotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class OneMain {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        System.out.println("Anot");
        Anot a=new Anot();
        Method[] f=a.getClass().getDeclaredMethods();
        for (Method s:f) {
            if(s.isAnnotationPresent(myAnot.class)){
                s.invoke(a);
            }
        }

    }
}

