package Home_Work_Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Target(ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface myAnot {
    String name();
    String type() default "string";
}
